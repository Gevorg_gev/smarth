using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication15.Models.DB;

namespace WebApplication15
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        [HttpGet("countByCountryAndCity")]
        public IActionResult GetCustumersCountByCountryAndCity()
        {
            Array custumersCountByCountryAndCity;
            using (var db = new NORTHWNDContext())
            {
                custumersCountByCountryAndCity = db.Customers
                                                   .GroupBy(x => new { x.Country, x.City })
                                                   .Select(x => new { x.Key.Country, x.Key.City, CutomersCount = x.Count() })
                                                   .OrderByDescending(x => x.CutomersCount)
                                                   .ToArray();
            }
            return Ok(custumersCountByCountryAndCity);
        } 
    }   
}
