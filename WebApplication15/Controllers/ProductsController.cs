using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication15.Models.DB;

namespace WebApplication15
{
    [ApiController]
    [Route("[controller]")]

    public class ProductsController : ControllerBase
    {
        [HttpGet("count/byCategory")]
        public IActionResult GetProductsCountByCategoory()
        {
            Array ProductsNumberByCategory;
            using (var db = new NORTHWNDContext())
            {
                ProductsNumberByCategory = db.Products
                                           .Join(db.Categories, x => x.CategoryId, y => y.CategoryId, (x, y) => new { y.CategoryName })
                                           .GroupBy(x => x.CategoryName)
                                           .Select(x => new { x.Key, ProductCount = x.Count() })
                                           .OrderByDescending(x => x.ProductCount)
                                           .ToArray(); 
            }
            return Ok(ProductsNumberByCategory);  
        } 

    }
}