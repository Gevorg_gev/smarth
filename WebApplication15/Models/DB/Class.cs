﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication15.Models.DB
{
    public partial class Class
    {
        public string Class1 { get; set; }
        public string Type { get; set; }
        public string Country { get; set; }
        public byte? Numguns { get; set; }
        public float? Bore { get; set; }
        public int? Displacement { get; set; }
    }
}
