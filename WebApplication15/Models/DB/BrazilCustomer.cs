﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication15.Models.DB
{
    public partial class BrazilCustomer
    {
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
    }
}
