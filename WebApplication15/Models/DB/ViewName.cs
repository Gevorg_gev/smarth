﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication15.Models.DB
{
    public partial class ViewName
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
