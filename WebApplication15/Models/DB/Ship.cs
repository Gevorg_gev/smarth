﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication15.Models.DB
{
    public partial class Ship
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public short? Launched { get; set; }
    }
}
