﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication15.Models.DB
{
    public partial class Insertion
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
